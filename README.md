# Grey Dawn

An attempt to create something.

 1. [The Base of Computation](/grey-dawn/chapters/cf.html)
 2. [The Spirit of Machine](/grey-dawn/chapters/sm.html)
 3. [The Function of Procedures](/grey-dawn/chapters/fp.html)
 4. [The Elegance of Classes](/grey-dawn/chapters/ec.html)
 5. [The Abstraction of Generics](/grey-dawn/chapters/ag.html)
 6. [Of Compilation and Interpretation](/grey-dawn/chapters/ci.html)
 7. [The Assemblage](/grey-dawn/chapters/a.html)
 8. [Higher and Higher](/grey-dawn/chapters/hh.html)

Copyright (c) 2019 Maximilien A. Cura
